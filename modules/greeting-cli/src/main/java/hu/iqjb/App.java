package hu.iqjb;


import hu.iqjb.mod.Greeting;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {
        Greeting greeting = new Greeting();
        System.out.println(greeting.sayHello());

    }
}
