package hu.iqjb;

public class MyBox extends Box {
    public MyBox(double width, double height, double depth) {
        super(width, height, depth);
    }

    @Override
    public double volume(){
        return 12;
    }

    @Override
    public void info() {

    }


    @Override
    public void print() {

    }

    @Override
    public void name() {

    }

    @Override
    public void demo1() {

    }
}
