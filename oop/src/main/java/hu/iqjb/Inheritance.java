package hu.iqjb;

public class Inheritance {

    public static void main(String[] args) {
        ModifiedBox modifiedBox = new ModifiedBox(1,2,3);
        volume(modifiedBox);

        Box box = new ModifiedBox(1,2,3);
        volume(box);

        Box boxWithColor = new BoxWithColor(1,2,
                3, 4);
        volume(boxWithColor);

        ((BoxWithColor)boxWithColor).setColor(22);
        System.out.println(box.getClass().getCanonicalName());
        if (box instanceof BoxWithColor){
            ((BoxWithColor)box).setColor(22);
        }

        MyBox myBox = new MyBox(1,2,3);
        System.out.println(myBox.volume());


        PrintableInterface printableInterface =
                new MyBox(1,2,3);
        printableInterface.print();

    }

    private static void volume(Box box){
        System.out.println(box.volume());
    }

}
