package hu.iqjb;

public class BoxWithColor extends Box {
    private int color;

    public int getColor() {
        return color;
    }

    public void setColor(int color) {
        this.color = color;
    }

    public BoxWithColor(double width, double height,
                        double depth, int color) {
        super(width, height, depth);
        this.color = color;
    }

    public double volume(){
        System.out.println("Modified2: ") ;
        return super.volume();
    }

    @Override
    public void info() {

    }

    @Override
    public void print() {

    }

    @Override
    public void name() {

    }

    @Override
    public void demo1() {

    }
}
