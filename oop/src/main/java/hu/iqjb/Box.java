package hu.iqjb;

public abstract class Box extends RootBox implements RootBoxInterface, PrintableInterface {
    private double width;
    private double height;
    private double depth;

    private static int instances;

    public static int getInstances(){
        return instances;
    }

    static {
        instances = 20;
    }

    public double getWidth() {
        return width;
    }

    public void setWidth(double width) {
        this.width = width;
    }

    public double getHeight() {
        return height;
    }

    public void setHeight(double height) {
        this.height = height;
    }

    public double getDepth() {
        return depth;
    }

    public void setDepth(double depth) {
        this.depth = depth;
    }



//    public static Box createBoxWithoutColor(double width, double height, double depth) {
//        return new Box(width, height, depth);
//    }

//    public static Box createDefaultBox(){
//        return new Box();
//    }

    public Box(double width, double height, double depth) {
        this.width = width;
        this.height = height;
        this.depth = depth;
        instances++;
    }

//    public Box(){
//        this(10,10,10);
//    }

    private void init(double width, double height, double depth) {
        this.width = width;
        this.height = height;
        this.depth = depth;
    }

    // compute and return volume
    public double volume() {
        System.out.printf(Box.instances + "");
        return this.width * this.height * this.depth;
    }

}
