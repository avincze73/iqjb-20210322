package hu.iqjb;

public class ModifiedBox extends Box {
    public ModifiedBox(double width, double height,
                       double depth) {
        super(width, height, depth);
    }

    public double volume(){
        System.out.println("Modified: ") ;
        return super.volume();
    }

    @Override
    public void info() {

    }

    @Override
    public void print() {

    }

    @Override
    public void name() {

    }

    @Override
    public void demo1() {

    }
}
