package hu.iqjb;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {
        List<Integer> integerList = new ArrayList<>();
        integerList.add(12);
        integerList.add(13);
        integerList.add(14);
        integerList.add(15);

        for (int i: integerList) {
            System.out.println(i);
        }


    }
}
