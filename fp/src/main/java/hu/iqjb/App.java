package hu.iqjb;

import java.util.List;
import java.util.function.*;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {
        int[] a = {1,2,3,4,5,6,7,8,9,10};
        System.out.println(accumulation(a, 0));

        System.out.println(accumulation2(a, 0, new Add(), new Positive()));
        System.out.println(accumulation2(a, 1, new Multiply(), new Even()));

        System.out.println(accumulation2(a, 1, new Multiply(),
                new Condition() {
                    @Override
                    public boolean test(int a) {
                        return a < 0;
                    }
                }
        ));


        System.out.println(accumulation2(a, 1,
                new Operation() {
                    @Override
                    public int calc(int a, int b) {
                        return a - b;
                    }
                },
                new Condition() {
                    @Override
                    public boolean test(int a) {
                        return a < 0;
                    }
                }
        ));

        System.out.println(accumulation3(a, 1,
                (c,  b) -> c - b,
                 c -> c < 0
        ));

        System.out.println(accumulation3(a, 1,
                MyAccum::add,
                MyAccum::even
        ));


        MyAccum myAccum = new MyAccum();
        System.out.println(accumulation3(a, 1,
                myAccum::add2,
                myAccum::even2
        ));

        Operation lo = ( int c, int  b) -> c - b;
        Operation2 lo2 = ( c,  b) -> c - b;

        Operation op = new Add();


        Condition lc = c -> c < 0;
        System.out.println(accumulation2(a, 1, lo, lc));




        Operation.print();

        MyClass myClass = new MyClass();
        myClass.f();

        Fruit banana = FruitFactory.create("banana");
        Fruit orange = FruitFactory.create("orange");

    }

    private static int accumulation(int[] a, int init){
        for (int i: a) {
            init += i;
        }
        return  init;
    }

    private static int accumulation2(int[] a, int init, Operation op,
                                     Condition co){
        for (int i: a) {
            if (co.test(i)){
                init = op.calc(init, i);
            }
        }
        return  init;
    }


    private static int accumulation3(int[] a, int init,
                                     IntBinaryOperator op,
                                     IntPredicate co){
        for (int i: a) {
            if (co.test(i)){
                init = op.applyAsInt(init, i);
            }
        }
        return  init;
    }

}


class MyAccum {
    public static int add(int a, int b){
        return a + b;
    }
    public static int multiply(int a, int b){
        return a * b;
    }
    public static boolean positive(int a){
        return a > 0;
    }
    public static boolean even(int a){
        return (a % 2) == 0;
    }

    public  int add2(int a, int b){
        return a + b;
    }
    public  int multiply2(int a, int b){
        return a * b;
    }
    public  boolean positive2(int a){
        return a > 0;
    }
    public  boolean even2(int a){
        return (a % 2) == 0;
    }
}