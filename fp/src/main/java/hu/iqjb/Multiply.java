package hu.iqjb;

public class Multiply implements Operation {
    @Override
    public int calc(int a, int b) {
        return a * b;
    }
}
