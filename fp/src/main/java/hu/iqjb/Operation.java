package hu.iqjb;

@FunctionalInterface
public interface Operation {

    int calc(int a, int b);
    default  double f (){
        g();
        return 0.0;
    }

    default  double h (){
        g();
        return 0.0;
    }
    static void print(){
        System.out.println("From static method");
    }

    private void g(){
    }
}
