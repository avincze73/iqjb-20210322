package hu.iqjb;

public class Add implements Operation{
    @Override
    public int calc(int a, int b) {
        return a + b;
    }

}
