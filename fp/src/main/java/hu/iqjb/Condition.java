package hu.iqjb;

public interface Condition {
    boolean test(int a);
}
