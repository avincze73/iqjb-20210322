package hu.iqjb;

public interface B extends A {
    default void f(){
        System.out.println("B.f()");
    }
}
