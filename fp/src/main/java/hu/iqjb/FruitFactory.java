package hu.iqjb;

import java.util.HashMap;
import java.util.Map;
import java.util.function.Supplier;

public class FruitFactory {
    public static Fruit create(String str){
        return map.get(str).get();
    }

    private static Map<String, Supplier<Fruit>> map
            = new HashMap<>();

    static {
        map.put("orange", Orange::new);
        map.put("banana", Banana::new);
    }
}
