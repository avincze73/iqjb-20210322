package hu.iqjb;

public class Positive implements Condition{
    @Override
    public boolean test(int a) {
        return a > 0;
    }
}
