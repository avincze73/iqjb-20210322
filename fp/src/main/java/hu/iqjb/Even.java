package hu.iqjb;

public class Even implements Condition{
    @Override
    public boolean test(int a) {
        return (a % 2) == 0;
    }
}
