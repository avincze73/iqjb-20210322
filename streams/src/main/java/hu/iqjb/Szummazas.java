/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.iqjb;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Function;
import java.util.stream.IntStream;
import java.util.stream.LongStream;
import java.util.stream.Stream;

/**
 *
 * @author avincze
 */
public class Szummazas {

    public static long VALUE = 30_000_000L;

    public static void main(String[] args) {
//
//        System.out.println(
//                "osszeadas: " + futasiIdo(Szummazas::osszeadas, VALUE));
//
//        System.out.println(
//                "osszeadas2: " + futasiIdo(Szummazas::osszeadas2, VALUE));
//
////        System.out.println(
////                "osszeadas2b: " + futasiIdo(Szummazas::osszeadas2b, VALUE));
////
//        System.out.println(
//                "osszeadas3: " + futasiIdo(Szummazas::osszeadas3, VALUE));
//
//        System.out.println(
//                "osszeadas4: " + futasiIdo(Szummazas::osszeadas4, VALUE));
//
//        System.out.println(
//                "osszeadas4b: " + futasiIdo(Szummazas::osszeadas4b, VALUE));

        System.out.println(
                "osszeadas5: " + futasiIdo(Szummazas::osszeadas5, VALUE));

        System.out.println(
                "osszeadas5b: " + futasiIdo(Szummazas::osszeadas5b, VALUE));

    }

    public static long osszeadas(long meret) {
        long result = 0L;
        for (int i = 1; i <= meret; i++) {
            result += i;
        }
        return result;
    }

    public static long osszeadas2(long meret) {

        return Stream.iterate(1L, i -> i + 1).limit(VALUE)
                .sequential()
                .reduce(Long::sum).get();
    }

    public static long osszeadas2b(long meret) {

        return Stream.iterate(1L, i -> i + 1).limit(VALUE)
                .parallel()
                .reduce(Long::sum).get();
    }

    public static long osszeadas3(long meret) {

        return LongStream.iterate(1L, i -> i + 1).limit(VALUE)
                .reduce((a, b) -> a + b).getAsLong();
    }

    public static long osszeadas4(long meret) {

        return LongStream.rangeClosed(1L, VALUE)
                .reduce((a, b) -> a + b).getAsLong();
    }

    public static long osszeadas4b(long meret) {

        return LongStream.rangeClosed(1L, VALUE)
                .parallel()
                .reduce((a, b) -> a + b).getAsLong();
    }
    
    public static long osszeadas5(long meret) {
        MySzumma mySzumma = new MySzumma();
        LongStream.rangeClosed(1L, VALUE)
                .forEach(mySzumma::noveles);
        return mySzumma.szumma;
    }
    
    public static long osszeadas5b(long meret) {
        MySzumma mySzumma = new MySzumma();
        LongStream.rangeClosed(1L, VALUE)
                .parallel()
                .forEach(mySzumma::noveles);
        return mySzumma.szumma;
    }

    private static <T, R> long futasiIdo(
            Function<T, R> fuggveny, T arg) {
        List<Long> result = new ArrayList<>();
        IntStream.rangeClosed(0, 10).forEach(
                i -> {
                    long start = System.currentTimeMillis();
                    R ret = fuggveny.apply(arg);
                    System.out.println(ret);
                    long end = System.currentTimeMillis();
                    result.add(end - start);
                }
        );
        return result.stream().mapToLong(l -> l).min().getAsLong();
    }

}

class MySzumma{
    public long szumma = 0L;
    
    public synchronized void noveles(long arg){
        szumma += arg;
    }
}
