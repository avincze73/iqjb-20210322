package hu.iqjb;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.function.Function;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {
        List<Integer> integerList = new ArrayList<>();
        integerList.add(1);
        integerList.add(2);
        integerList.add(3);
        integerList.add(4);
        integerList.add(5);
        integerList.add(6);
        integerList.add(6);
        integerList.add(6);
        integerList.add(6);
        integerList.add(7);
        integerList.add(8);
        integerList.stream()
                .filter(a->(a%2)==0)
                .distinct()
                .skip(2)
                .forEach(System.out::println);
         integerList.stream()
                .skip(10)
                .findAny();

        int sum = integerList.stream()
                .reduce(1, Integer::sum );

        List<String> stringList = new ArrayList<>();
        stringList.add("aa");
        stringList.add("bbb");
        stringList.add("cccc");
        stringList.add("ddddd");
        stringList.add("eeeeee");

        stringList.stream()
                .map(String::length)
                .forEach(System.out::println);




    }
}
