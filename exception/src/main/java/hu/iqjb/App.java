package hu.iqjb;


/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {
        System.out.println( "Hello World!" );

        try {
            checkedException();
            int a = 10;
            a++;
        } catch (MyException | IllegalArgumentException myException) {
            myException.printStackTrace();
        } catch (Exception e) {

        } finally {

        }
        uncheckedException();
    }

    private static void checkedException()
            throws MyException {
        throw new MyException();
    }

    private static void uncheckedException(){
        throw new NumberFormatException();
    }
}
