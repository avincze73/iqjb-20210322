package hu.iqjb;

public class GenStack<T> {

    private Object[] db;
    private int top;


    public GenStack() {
        top = -1;
        db = new Object[0];
    }

    private void resize(){
        int newSize = db.length * 2 + 1;
        Object[] temp = new Object[newSize];
        for (int i = 0; i < db.length; i++) {
            temp[i] = db[i];
        }
        db = temp;
    }

    public T pop() throws StackIsEmptyException {
        if (top == -1) {
            throw new StackIsEmptyException();
        }
        Object result = db[top];
        top--;
        return (T)result;
    }

    public void push(T arg){
        if (top == db.length - 1){
            resize();
        }
        db[++top] = arg;
    }
}
