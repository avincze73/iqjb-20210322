package hu.iqjb;

/**
 * Hello world!
 */
public class App {
    public static void main(String[] args) {

        GenStack<Integer> intStack = new GenStack<>();
        GenStack<String> stringStack = new GenStack<>();
        for (int i = 0; i < 100; i++) {
            intStack.push(i);
            stringStack.push(Integer.toString(i));
        }

        try {
            for (int i = 0; i < 110; i++) {
                System.out.println(intStack.pop());
            }
        } catch (StackIsEmptyException e) {
            e.printStackTrace();
        }

        try {
            for (int i = 0; i < 110; i++) {
                System.out.println(stringStack.pop());
            }
        } catch (StackIsEmptyException e) {
            e.printStackTrace();
        }
    }
}
