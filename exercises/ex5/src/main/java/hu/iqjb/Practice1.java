package hu.iqjb;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static java.util.stream.Collectors.counting;
import static java.util.stream.Collectors.groupingBy;

public class Practice1 {
    public static void main(String[] args) {
        try (Stream<String> lines =
                     Files.lines(Paths.get("/Users/avincze/git/iqjb-20210322/exercises/dictionary.txt"))) {

//            Map<Integer, List<String>> result =
//            lines.collect(groupingBy(String::length));
//            result.values().forEach(System.out::println);

            lines.collect(groupingBy(String::length, counting()))
                    .forEach((k,v)-> System.out.println(k + ": " + v));



        } catch (IOException ex){
            ex.printStackTrace();
        }
    }
}
