package hu.iqjb;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static java.util.stream.Collectors.counting;
import static java.util.stream.Collectors.groupingBy;

public class Practice7 {
    public static void main(String[] args) {
        try (Stream<String> lines =
                     Files.lines(Paths.get("/Users/avincze/git/iqjb-20210322/exercises/dictionary.txt"))) {


            String word = "secure";
            String ordered = order(word);
            lines.filter(w -> ordered.equals(order(w)))
                    .forEach(System.out::println);



        } catch (IOException ex){
            ex.printStackTrace();
        }
    }


    private static String order(String word){
        return word.chars().sorted()
                .mapToObj(String::valueOf)
                .collect(Collectors.joining());
    }

}
