package hu.iqjb;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static java.util.stream.Collectors.*;

public class Practice10 {
    public static void main(String[] args) {
        try (Stream<String> lines =
                     Files.lines(Paths.get("/Users/avincze/git/iqjb-20210322/exercises/dictionary.txt"))) {


            lines
                    .collect(groupingBy(w->order(w), counting()))
                    .values().stream()
                    .collect(partitioningBy(n->n==1, counting()))
                            .forEach((k,v) -> System.out.println(k + ": " + v));


        } catch (IOException ex){
            ex.printStackTrace();
        }
    }


    private static String order(String word){
        return word.chars().sorted()
                .mapToObj(String::valueOf)
                .collect(Collectors.joining());
    }

}
