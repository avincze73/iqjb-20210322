package hu.iqjb;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {

        for (int i = 0; i < 100; i++) {
            IntStack.getInstance().push(i);
        }

        for (int i = 0; i < 100; i++) {
            System.out.println(IntStack.getInstance().pop());
        }
    }
}
