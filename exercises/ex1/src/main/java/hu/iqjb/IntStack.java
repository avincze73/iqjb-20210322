package hu.iqjb;

public class IntStack {

    private int[] db;
    private int top;

    private static IntStack onlyInstance;

    private IntStack() {
        top = -1;
        db = new int[0];
    }

    public static IntStack getInstance(){
        if (onlyInstance == null){
            onlyInstance = new IntStack();
        }
        return  onlyInstance;
    }

    private void resize(){
        int newSize = db.length * 2 + 1;
        int[] temp = new int[newSize];
        for (int i = 0; i < db.length; i++) {
            temp[i] = db[i];
        }
        db = temp;
    }

    public int pop(){
        int result = db[top];
        top--;
        return result;
    }

    public void push(int arg){
        if (top == db.length - 1){
            resize();
        }
        db[++top] = arg;
    }
}
