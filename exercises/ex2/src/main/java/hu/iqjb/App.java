package hu.iqjb;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {
        Shape circle = new Circle(2);
        info(circle);

        Shape rectangle = new Rectangle(2,3);
        info(rectangle);

        Shape square = new Square(4);
        info(square);

        Shape[] shapes = {
                new Square(4),
                new Rectangle(2,3),
                new Circle(2)
        };
        for (Shape shape: shapes) {
            info(shape);
        }


    }

    private static void info(Shape arg){
        System.out.println(arg.area() +
                " - " + arg.perim());
    }

}
