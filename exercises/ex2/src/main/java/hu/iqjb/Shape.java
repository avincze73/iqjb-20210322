package hu.iqjb;

public interface Shape {
    double area();
    double perim();
}
